<h1 align="center">Based on Olauncher and Olauncher CF. But with Material You colors support and a few font tweaks.</h1>  
<h3 align="center">Olauncher is a minimal app launcher for Android.</h3>  

# Forked with extra features

- Removed clutter, like ads and links
- You can rename apps in the app-drawer _(Renaming apps on the home screen is already supported. Just long-click on an app on the home screen and start typing)_
- Ability to change the app when clicking on the clock
- Ability to change the app when clicking on the date
- Align clock independently of home apps
- Change alignment of apps in app-drawer
- Change font size
- A lot of people have translated the app to the following languages. Many thanks to you ❤️
  - Chinese
  - Croatian
  - English
  - Estonian
  - French
  - German
  - Greek
  - Indonesian
  - Italian
  - Korean
  - Persian
  - Portuguese (European)
  - Russian
  - Spanish
  - Swedish
  - Turkish
- Removed internet permission

## License

**Olauncher M is under open source GPL3 license, meaning you can use, study, change and share it at will.**
Copyleft ensures it stays that way. From the full source, anyone can build, fork and use as you wish

* Olauncher M does not have network access.
* Olauncher M does not collect or transmit any data in any way whatsoever.

## Permissions

Olauncher M uses the following permissions:

- `android.permission.EXPAND_STATUS_BAR`
	- Allows an application to expand or collapse the status bar.
- `android.permission.QUERY_ALL_PACKAGES`
	- Allows query of any normal app on the device, regardless of manifest declarations. Used to show the apps list.
- `android.alarm.permission.SET_ALARM`
	- Allows an application to broadcast an Intent to set an alarm for the user. Used to open the default alarm app if no other clock app is set in the settings.
- `android.permission.REQUEST_DELETE_PACKAGES`
	- Required for issuing the request to remove packages. This does not allow the app to remove apps directly; this only gives the permission to issue the request.
